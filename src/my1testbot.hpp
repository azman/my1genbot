//------------------------------------------------------------------------------
#ifndef __MY1TESTBOTHPP__
#define __MY1TESTBOTHPP__
//------------------------------------------------------------------------------
#include "my1bnnbot.hpp"
//------------------------------------------------------------------------------
#define VIEW_HEIGHT 62
#define VIEW_WIDTH 82
// hue constant for yellow
#define TARGET_HUE 84
#define HUE_THRESH 10
// hue defined as obstacle
#define OBSTACLE_HUE NO_HUE
//------------------------------------------------------------------------------
// constant for PSD range sensor
#define SAFE_PSDRANGE 300
//------------------------------------------------------------------------------
// dummy robot implementation class
class my1TestBot : public my1BnnBot
{
public:
	my1TestBot();
	// processing functions
	virtual void CaptureView(void);
	virtual void CaptureSens(void);
	virtual void MoveDrive(void);
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
