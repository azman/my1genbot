//------------------------------------------------------------------------------
#include "my1bhvbot.hpp"
//------------------------------------------------------------------------------
#include <cstdlib>
using std::rand;
using std::srand;
#include <ctime>
using std::time;
//------------------------------------------------------------------------------
my1Behavior::my1Behavior()
{
	mBehaveID = RBHV_ERROR; // child should override ID
	mFactor = 1.0; // output as it is
	mActive = false; // can be override
	mInput = 0x0;
	mSense = 0x0;
	mDrive.turn = TURN_0;
	mDrive.drive = MOVE_0;
	mDrive.dist = 0.0;
	mDrive.angle = 0.0;
}
//------------------------------------------------------------------------------
int my1Behavior::GetBehaveID(void)
{
	return mBehaveID;
}
//------------------------------------------------------------------------------
void my1Behavior::SetFactor(float aValue)
{
	if(aValue>=0.0f&&aValue<1.0f)
		mFactor = aValue;
}
//------------------------------------------------------------------------------
float my1Behavior::GetFactor(void)
{
	return mFactor;
}
//------------------------------------------------------------------------------
bool my1Behavior::IsActive(void)
{
	return mActive;
}
//------------------------------------------------------------------------------
void my1Behavior::Input(roboview* view)
{
	mInput = view;
}
//------------------------------------------------------------------------------
roboview* my1Behavior::Input(void)
{
	return mInput;
}
//------------------------------------------------------------------------------
void my1Behavior::Sense(robosens* sensor)
{
	mSense = sensor;
}
//------------------------------------------------------------------------------
robosens* my1Behavior::Sense(void)
{
	return mSense;
}
//------------------------------------------------------------------------------
robomove& my1Behavior::Drive(void)
{
	return mDrive;
}
//------------------------------------------------------------------------------
void my1Behavior::Evaluate(void)
{
	// default is simple avoid behavior - mActive always false
	if(!mInput->info.center&&!mInput->info.sidel&&!mInput->info.sider)
	{
		mDrive.drive = MOVE_F;
		mDrive.turn = TURN_0;
	}
	else if(!mInput->info.center&&!mInput->info.sider&&mSense->r_safe)
	{
		mDrive.drive = MOVE_0;
		mDrive.turn = TURN_R;
	}
	else if(!mInput->info.center&&!mInput->info.sidel&&mSense->l_safe)
	{
		mDrive.drive = MOVE_0;
		mDrive.turn = TURN_L;
	}
	else if(!mInput->info.center) // both sides has obstacle
	{
		mDrive.drive = MOVE_F;
		mDrive.turn = TURN_0;
	}
	else
	{
		if(mSense->r_safe) mDrive.turn = TURN_R;
		else if(mSense->l_safe) mDrive.turn = TURN_L;
		else mDrive.turn = TURN_0;
		mDrive.drive = (mDrive.turn==TURN_0) ? MOVE_R : MOVE_0;
	}
	// assign drive parameters
	mDrive.dist = mDrive.drive*1.0; // no scaling
	mDrive.angle = mDrive.turn*1.0;
}
//------------------------------------------------------------------------------
void my1BhvAvoid::Evaluate(void)
{
	// assume this behavior is active first!
	mActive = true;
	if(!mInput->info.center)
	{
		if(!mInput->info.sidel&&!mInput->info.sider)
		{
			// no obstacle detected!
			mDrive.drive = MOVE_F;
			mDrive.turn = TURN_0;
			mActive = false; // can be override
		}
		else if(!mInput->info.sider&&mSense->r_safe)
		{
			mDrive.drive = MOVE_0;
			if(mDrive.turn==TURN_L) // continue - to avoid local minima
				mDrive.turn = TURN_L;
			else
				mDrive.turn = TURN_R;
		}
		else if(!mInput->info.sidel&&mSense->l_safe)
		{
			mDrive.drive = MOVE_0;
			if(mDrive.turn==TURN_R) // continue - to avoid local minima
				mDrive.turn = TURN_R;
			else
				mDrive.turn = TURN_L;
		}
		else
		{
			// only center has no obstacle? try forward?
			mDrive.drive = MOVE_F;
			mDrive.turn = TURN_0;
		}
	}
	else
	{
		// obstacle ahead?
		if(mSense->r_safe&&mSense->l_safe)
			mDrive.turn = rand()%2?TURN_R:TURN_L; // random direction
		else if(mSense->r_safe) mDrive.turn = TURN_R;
		else if(mSense->l_safe) mDrive.turn = TURN_L;
		else mDrive.turn = TURN_0;
		mDrive.drive = (mDrive.turn==TURN_0) ? MOVE_R : MOVE_0;
	}
	// assign drive parameters
	mDrive.dist = mDrive.drive*0.5; // half normal distance
	mDrive.angle = mDrive.turn*1.0;
}
//------------------------------------------------------------------------------
void my1BhvScout::Evaluate(void)
{
	mActive = true;
	if(mInput->info.bview)
	{
		mDrive.drive = MOVE_F;
		mDrive.turn = mInput->info.bside;
		mActive = false; // can be override
	}
	else
	{
		// basically turn away fom obstacle
		if(!mSense->r_safe&&mSense->l_safe)
			mDrive.turn = TURN_L;
		else if(mSense->r_safe&&!mSense->l_safe)
			mDrive.turn = TURN_R;
		else
		{
			// 1/8 chance turning
			mDrive.turn = rand()%2? TURN_R : TURN_L;
			mDrive.turn = rand()%4==0? mDrive.turn : TURN_0;
		}
		mDrive.drive = mDrive.turn? MOVE_0 : MOVE_F; // don't move if turning
	}
	// assign drive parameters
	mDrive.dist = mDrive.drive*1.0;
	mDrive.angle = mDrive.turn*0.5; // only turn half as much
}
//------------------------------------------------------------------------------
void my1BhvDrive::Evaluate(void)
{
	mActive = true;
	if(mInput->info.bnear) // assume ball IS in view!!!
	{
		mDrive.drive = MOVE_0;
		mDrive.turn = mInput->info.bside;
		if(!mDrive.turn) mActive = false; // can be override when stopped?
	}
	else
	{
		// same as scout behavior when ball is in view
		mDrive.drive = MOVE_F;
		mDrive.turn = mInput->info.bside;
	}
	// assign drive parameters
	mDrive.dist = mDrive.drive*0.5;
	mDrive.angle = mDrive.turn*0.25;
}
//------------------------------------------------------------------------------
my1BhvBot::my1BhvBot(int aViewX, int aViewY, int aRange)
	: my1VisBot(aViewX, aViewY, aRange)
{
	mDefaultBehavior.Input(&mView);
	mDefaultBehavior.Sense(&mSens);
}
//------------------------------------------------------------------------------
int my1BhvBot::BehaviorCount(void)
{
	return mBhvList.size();
}
//------------------------------------------------------------------------------
int my1BhvBot::AddBehavior(my1Behavior* aBehavior)
{
	list<my1Behavior*>::iterator pBehavior = mBhvList.begin();
	int cIndex = 0;

	aBehavior->Input(&mView);
	aBehavior->Sense(&mSens);

	while(pBehavior!=mBhvList.end())
	{
		if((*pBehavior)->GetBehaveID()>aBehavior->GetBehaveID())
			break;
		cIndex++; pBehavior++;
	}
	mBhvList.insert(pBehavior,aBehavior);

	return cIndex;
}
//------------------------------------------------------------------------------
int my1BhvBot::DelBehavior(int anIndex)
{
	list<my1Behavior*>::iterator pBehavior = mBhvList.begin();
	int cIndex = 0;

	while(pBehavior!=mBhvList.end()&&cIndex<anIndex)
	{
		cIndex++; pBehavior++;
	}

	if(cIndex==anIndex) mBhvList.erase(pBehavior);
	else cIndex = -1;

	return cIndex;
}
//------------------------------------------------------------------------------
my1Behavior* my1BhvBot::GetBehavior(int anIndex)
{
	list<my1Behavior*>::iterator pBehavior = mBhvList.begin();
	int cIndex = 0;
	my1Behavior* qBehavior = 0x0;

	while(pBehavior!=mBhvList.end()&&cIndex<anIndex)
	{
		cIndex++; pBehavior++;
	}

	if(cIndex==anIndex) qBehavior = *pBehavior;

	return qBehavior;
}
//------------------------------------------------------------------------------
int my1BhvBot::Evaluate(void)
{
	my1Behavior* pSelect = &mDefaultBehavior;
	list<my1Behavior*>::iterator pBehavior = mBhvList.begin();

	if(!mBhvList.size())
		mDefaultBehavior.Evaluate();
	while(pBehavior!=mBhvList.end())
	{
		pSelect = (my1Behavior*) *pBehavior;
		pSelect->Evaluate();
		if(pSelect->IsActive()) break;
		pBehavior++;
	}
	this->Drive(pSelect->Drive());

	return pSelect->GetBehaveID();
}
//------------------------------------------------------------------------------
