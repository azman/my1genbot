//------------------------------------------------------------------------------
#ifndef __MY1MOBBOTHPP__
#define __MY1MOBBOTHPP__
//------------------------------------------------------------------------------
// constant for robot driving - assigned to robomove drive/turn
#define MOVE_0 0
#define MOVE_F 1
#define MOVE_R -1
#define TURN_0 0
#define TURN_L 1
#define TURN_R -1
//------------------------------------------------------------------------------
// drive constants
typedef struct _roboconf
{
	float drv_speed, ang_speed; // how fast
	float dist, turn; // how far per count
}
roboconf;
//------------------------------------------------------------------------------
// drive instructions
typedef struct _robomove
{
	int drive, turn; // dist/turn counts
	float dist, angle; // sub-scale for dist/turn counts
}
robomove;
//------------------------------------------------------------------------------
// class for basic mobile robot
class my1MobBot
{
protected:
	roboconf mConst;
	robomove mDrive;
public:
	my1MobBot();
	void Const(roboconf&);
	roboconf& Const(void);
	void Drive(robomove&);
	robomove& Drive(void);
	// movement check - true if robomove counts are zero (stopped)
	virtual int ZeroDrive(void);
	// drive methods - must be override by actual robot class
	virtual void MoveDrive(void) = 0; // abstract class!
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
