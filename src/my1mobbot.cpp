//------------------------------------------------------------------------------
#include "my1mobbot.hpp"
//------------------------------------------------------------------------------
my1MobBot::my1MobBot()
{
	// should be re-initialized by actual robot classes
	mConst.drv_speed = 1.0f;
	mConst.ang_speed = 1.0f;
	mConst.dist = 1.0f;
	mConst.turn = 1.0f;
	// initialize drive mode - stop!
	mDrive.drive = MOVE_0;
	mDrive.turn = TURN_0;
	mDrive.dist = 0.0f;
	mDrive.angle = 0.0f;
}
//------------------------------------------------------------------------------
void my1MobBot::Const(roboconf& aConst)
{
	mConst = aConst;
}
//------------------------------------------------------------------------------
roboconf& my1MobBot::Const(void)
{
	return mConst;
}
//------------------------------------------------------------------------------
void my1MobBot::Drive(robomove& aDrive)
{
	mDrive = aDrive;
}
//------------------------------------------------------------------------------
robomove& my1MobBot::Drive(void)
{
	return mDrive;
}
//------------------------------------------------------------------------------
int my1MobBot::ZeroDrive(void)
{
	return (mDrive.turn==MOVE_0&&mDrive.turn==TURN_0) ? 1 : 0;
}
//------------------------------------------------------------------------------
