//------------------------------------------------------------------------------
#ifndef __MY1NEURALHPP__
#define __MY1NEURALHPP__
//------------------------------------------------------------------------------
#include <vector>
using std::vector;
//------------------------------------------------------------------------------
class my1Neuron; // forward declaration
//------------------------------------------------------------------------------
struct my1Link // c++ struct
{
	my1Neuron* mInput;
	float mWeight;
	my1Link(my1Neuron* aNode=0x0, float aWeight=0.0);
};
//------------------------------------------------------------------------------
class my1Neuron
{
protected: // child can access this
	vector<my1Link> mLinks;
	bool mIsInput; // input node shouldn't evaluate
	float mValue; // register output value
public:
	my1Neuron();
	my1Neuron(const my1Neuron&); // copy constructor (vectors not copied!)
	virtual ~my1Neuron();
	void Clear(void);
	void AsInputNode(bool);
	bool IsInputNode(void);
	void AddInput(my1Neuron*);
	int CountInput(void);
	void SetWeight(int,float);
	float GetWeight(int);
	void SetWeight(float*); // user must check size
	void GetWeight(float*); // user must create space
	void SetValue(float); // only input node use this
	float GetValue(void);
	virtual float Evaluate(void); // override to change eval method
};
//------------------------------------------------------------------------------
typedef vector<my1Neuron> my1Layer;
//------------------------------------------------------------------------------
class my1Neural
{
protected: // in case we want to create child class
	vector<my1Layer> mLayers; // points to externally created layer(s)!
	int mWeightCount; // class-level copy for weight count
public:
	my1Neural(int,int); // input node count, output node count
	virtual ~my1Neural();
	int InsertLayer(int); // hidden node count
	int ResetLayers(int,int); // input node count, output node count
	int WeightCount(void);
	int LayerCount(void);
	int NodeCount(int); // arg: layer index
	my1Neuron* Node(int,int); // layer index, node index
	void SetWeight(float*); // user must check size
	void GetWeight(float*); // user must create space
	int CopyWeight(my1Neural*);
	void SetInput(float*); // user must check size
	void GetOutput(float*); // user must create space
	float GetOutput(int);
	int CreateLinks(void);
	int RemoveLinks(void);
	virtual void Evaluate(void); // MLP feedforward execution
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
