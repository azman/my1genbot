//------------------------------------------------------------------------------
#ifndef __MY1BHVBOTH__
#define __MY1BHVBOTH__
//------------------------------------------------------------------------------
#include "my1visbot.hpp"
//------------------------------------------------------------------------------
#include <list>
using std::list;
//------------------------------------------------------------------------------
// constants for behavior id
#define RBHV_ERROR 0x00
#define RBHV_AVOID 0x01
#define RBHV_SCOUT 0x02
#define RBHV_DRIVE 0x04
//------------------------------------------------------------------------------
class my1Behavior
{
protected:
	int mBehaveID; // also serves as priority (0 is root!)
	float mFactor;
	bool mActive;
	roboview* mInput;
	robosens* mSense;
	robomove mDrive;
public:
	my1Behavior();
	int GetBehaveID(void);
	void SetFactor(float); // for weighted summation
	float GetFactor(void);
	bool IsActive(void); // for behavior selection (priority)
	void Input(roboview*);
	roboview* Input(void);
	void Sense(robosens*);
	robosens* Sense(void);
	robomove& Drive(void);
	virtual void Evaluate(void);
};
//------------------------------------------------------------------------------
class my1BhvAvoid : public my1Behavior
{
public:
	my1BhvAvoid(){ mBehaveID = RBHV_AVOID; }
	virtual void Evaluate(void);
};
//------------------------------------------------------------------------------
class my1BhvScout : public my1Behavior
{
public:
	my1BhvScout(){ mBehaveID = RBHV_SCOUT; }
	virtual void Evaluate(void);
};
//------------------------------------------------------------------------------
class my1BhvDrive : public my1Behavior
{
public:
	my1BhvDrive(){ mBehaveID = RBHV_DRIVE; }
	virtual void Evaluate(void);
};
//------------------------------------------------------------------------------
// class for behavior robots
class my1BhvBot : public my1VisBot
{
protected:
	list<my1Behavior*> mBhvList; // behaviors created externally
	my1Behavior mDefaultBehavior;
public:
	my1BhvBot(int,int,int); // x,y,range
	virtual ~my1BhvBot(){}
	int BehaviorCount(void);
	int AddBehavior(my1Behavior*);
	int DelBehavior(int);
	my1Behavior* GetBehavior(int);
	virtual int Evaluate(void);
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
