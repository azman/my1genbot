//------------------------------------------------------------------------------
#ifndef __MY1VISBOTHPP__
#define __MY1VISBOTHPP__
//------------------------------------------------------------------------------
#include "my1mobbot.hpp"
//------------------------------------------------------------------------------
#define NO_HUE -1
//------------------------------------------------------------------------------
#define OBJECT_CENTER TURN_0
#define OBJECT_LEFT TURN_L
#define OBJECT_RIGHT TURN_R
//------------------------------------------------------------------------------
typedef struct _targetinfo
{
	int hue, thresh; // color-based target
}
targetinfo;
//------------------------------------------------------------------------------
typedef struct _targetview
{
	int left, right, middle;
	int horizon, weight;
}
targetview;
//------------------------------------------------------------------------------
typedef struct _viewinfo
{
	int bview, bside, bnear; // target info
	int sidel, sider, center; // path info
	int bpos; // bside raw value
}
viewinfo;
//------------------------------------------------------------------------------
typedef struct _roboview
{
	targetinfo target;
	targetview viewer;
	viewinfo info;
}
roboview;
//------------------------------------------------------------------------------
typedef struct _robosens
{
	int f_safe, r_safe, l_safe; // range sensors - <f>ront, <r>ight, <l>eft
	int f_dist, r_dist, l_dist;
	int saferange, valid;
}
robosens;
//------------------------------------------------------------------------------
// class for mobile robot with vision & range sensor
class my1VisBot : public my1MobBot
{
protected:
	int mViewX, mViewY;
	void* mBuff; // should be pointer to image[mViewY][mViewX][3]
	roboview mView; // view info
	robosens mSens; // range info
public:
	my1VisBot(int,int,int); // x,y,range
	robosens& Sens(void);
	roboview& View(void);
	// update viewinfo based on target color requires valid mBuff
	virtual void ColorDirect(void);
	// capture functions - need to be defined by actual robots
	virtual void CaptureView(void) = 0;
	virtual void CaptureSens(void) = 0;
	// utility function - one (copy) for all
	static int RGB2Hue(unsigned char,unsigned char,unsigned char);
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
