//------------------------------------------------------------------------------
#include "my1neural.hpp"
//------------------------------------------------------------------------------
#include <cstdlib>
using std::rand;
#include <cmath>
using std::exp;
//------------------------------------------------------------------------------
my1Link::my1Link(my1Neuron* aNode, float aWeight)
{
	mInput = aNode; mWeight = aWeight;
}
//------------------------------------------------------------------------------
my1Neuron::my1Neuron()
{
	mIsInput = false;
	mValue = 0.0;
}
//------------------------------------------------------------------------------
my1Neuron::my1Neuron(const my1Neuron& aNeuron)
{
	my1Neuron& cNode = const_cast<my1Neuron&>(aNeuron);
	mIsInput = cNode.IsInputNode();
	mValue = cNode.GetValue();
}
//------------------------------------------------------------------------------
my1Neuron::~my1Neuron()
{
	Clear();
}
//------------------------------------------------------------------------------
void my1Neuron::Clear(void)
{
	mLinks.clear();
}
//------------------------------------------------------------------------------
void my1Neuron::AsInputNode(bool asInput)
{
	mIsInput = asInput;
}
//------------------------------------------------------------------------------
bool my1Neuron::IsInputNode(void)
{
	return mIsInput;
}
//------------------------------------------------------------------------------
void my1Neuron::AddInput(my1Neuron *aNode)
{
	float cValue = (float) rand()/(RAND_MAX);
	cValue = (cValue*2.0)-1.0; /* random weight, range -1 to 1 */
	mLinks.push_back(my1Link(aNode,cValue));
}
//------------------------------------------------------------------------------
int my1Neuron::CountInput(void)
{
	return mLinks.size();
}
//------------------------------------------------------------------------------
void my1Neuron::SetWeight(int anIndex, float aWeight)
{
	mLinks[anIndex].mWeight = aWeight;
}
//------------------------------------------------------------------------------
float my1Neuron::GetWeight(int anIndex)
{
	return mLinks[anIndex].mWeight;
}
//------------------------------------------------------------------------------
void my1Neuron::SetWeight(float* aWeight)
{
	int cCount = mLinks.size();
	for(int cLoop=0;cLoop<cCount;cLoop++)
		mLinks[cLoop].mWeight = aWeight[cLoop];
}
//------------------------------------------------------------------------------
void my1Neuron::GetWeight(float* aWeight)
{
	int cCount = mLinks.size();
	for(int cLoop=0;cLoop<cCount;cLoop++)
		aWeight[cLoop] = mLinks[cLoop].mWeight;
}
//------------------------------------------------------------------------------
void my1Neuron::SetValue(float aValue)
{
	if(mIsInput)
		mValue = aValue;
}
//------------------------------------------------------------------------------
float my1Neuron::GetValue(void)
{
	return mValue;
}
//------------------------------------------------------------------------------
float my1Neuron::Evaluate(void)
{
	int cCount = mLinks.size();
	my1Neuron* pNode; float cWeight;
	mValue = 0.0;
	for(int cLoop=0;cLoop<cCount;cLoop++)
	{
		pNode = mLinks[cLoop].mInput;
		cWeight = mLinks[cLoop].mWeight;
		mValue += pNode->GetValue()*cWeight;
	}
	// sigmoid function
	mValue = (float) 1 / (1 + exp(-mValue));

	return mValue;
}
//------------------------------------------------------------------------------
void FillLayer(my1Layer* aLayer, int aCount, bool anInput)
{
	my1Neuron cNeuron;
	if(anInput) cNeuron.AsInputNode(true);
	for(int cLoop=0;cLoop<aCount;cLoop++)
		aLayer->push_back(cNeuron);
}
//------------------------------------------------------------------------------
void ClearLinks(my1Layer* aLayer)
{
	int cCount = aLayer->size();
	for(int cLoop=0;cLoop<cCount;cLoop++)
		(*aLayer)[cLoop].Clear();
}
//------------------------------------------------------------------------------
my1Neural::my1Neural(int aCountI, int aCountO)
{
	// prepare I/O layers
	ResetLayers(aCountI,aCountO);
	// initialize weight count
	mWeightCount = 0;
}
//------------------------------------------------------------------------------
my1Neural::~my1Neural()
{
	mLayers.clear(); // not really needed?
}
//------------------------------------------------------------------------------
int my1Neural::InsertLayer(int aCount)
{
	// create hidden layer
	my1Layer cHidden;
	my1Neuron cNode;
	for(int cLoop=0;cLoop<aCount;cLoop++)
		cHidden.push_back(cNode);

	int cIndex = mLayers.size()-1, cLoop = 0;
	vector<my1Layer>::iterator iLayer = mLayers.begin();
	while(iLayer!=mLayers.end()&&cLoop<cIndex)
	{
		iLayer++; cLoop++;
	}
	if(iLayer==mLayers.end())
		return -1; // can never insert as last layer!
	mLayers.insert(iLayer,cHidden);
	return cLoop;
}
//------------------------------------------------------------------------------
int my1Neural::ResetLayers(int aCountI, int aCountO)
{
	my1Layer cDummy;
	my1Neuron cNode;
	mLayers.clear();
	// create input layer
	cNode.AsInputNode(true);
	for(int cLoop=0;cLoop<aCountI;cLoop++)
		cDummy.push_back(cNode);
	mLayers.push_back(cDummy);
	// create output layer
	cDummy.clear();
	cNode.AsInputNode(false);
	for(int cLoop=0;cLoop<aCountO;cLoop++)
		cDummy.push_back(cNode);
	mLayers.push_back(cDummy);
	return mLayers.size();
}
//------------------------------------------------------------------------------
int my1Neural::WeightCount(void)
{
	return mWeightCount;
}
//------------------------------------------------------------------------------
int my1Neural::LayerCount(void)
{
	return mLayers.size();
}
//------------------------------------------------------------------------------
int my1Neural::NodeCount(int anIndex)
{
	return mLayers[anIndex].size();
}
//------------------------------------------------------------------------------
my1Neuron* my1Neural::Node(int aLayer, int aNode)
{
	return &mLayers[aLayer][aNode];
}
//------------------------------------------------------------------------------
void my1Neural::SetWeight(float* aWeight)
{
	float *pWeight = aWeight; int cCount = mLayers.size();
	for(int cIndex=1;cIndex<cCount;cIndex++) // skip input layer
	{
		int cCount1 = mLayers[cIndex].size();
		for(int cLoop=0;cLoop<cCount1;cLoop++)
		{
			mLayers[cIndex][cLoop].SetWeight(pWeight);
			pWeight += mLayers[cIndex][cLoop].CountInput();
		}
	}
}
//------------------------------------------------------------------------------
void my1Neural::GetWeight(float* aWeight)
{
	float *pWeight = aWeight; int cCount = mLayers.size();
	for(int cIndex=1;cIndex<cCount;cIndex++)
	{
		int cCount1 = mLayers[cIndex].size();
		for(int cLoop=0;cLoop<cCount1;cLoop++)
		{
			mLayers[cIndex][cLoop].GetWeight(pWeight);
			pWeight += mLayers[cIndex][cLoop].CountInput();
		}
	}
}
//------------------------------------------------------------------------------
int my1Neural::CopyWeight(my1Neural* aNeural)
{
	if(mWeightCount!=aNeural->WeightCount())
		return -1;

	int cCount = mLayers.size();
	for(int cIndex=1;cIndex<cCount;cIndex++)
	{
		int cCount1 = mLayers[cIndex].size();
		for(int cLoop=0;cLoop<cCount1;cLoop++)
		{
			my1Neuron* cNeuron = aNeural->Node(cIndex,cLoop);
			my1Neuron* dNeuron = &mLayers[cIndex][cLoop];
			int cCount2 = cNeuron->CountInput();
			for(int cLoop1=0;cLoop1<cCount2;cLoop1++)
			{
				float cWeight = cNeuron->GetWeight(cLoop1);
				dNeuron->SetWeight(cLoop1,cWeight);
			}
		}
	}

	return 0;
}
//------------------------------------------------------------------------------
void my1Neural::SetInput(float* aVector)
{
	int cCount = mLayers[0].size();
	for(int cLoop=0;cLoop<cCount;cLoop++)
	{
		mLayers[0][cLoop].SetValue(aVector[cLoop]);
	}
}
//------------------------------------------------------------------------------
void my1Neural::GetOutput(float* aVector)
{
	int cIndex = mLayers.size()-1;
	int cCount = mLayers[cIndex].size();
	for(int cLoop=0;cLoop<cCount;cLoop++)
	{
		aVector[cLoop] = mLayers[cIndex][cLoop].GetValue();
	}
}
//------------------------------------------------------------------------------
float my1Neural::GetOutput(int anIndex)
{
	return mLayers[mLayers.size()-1][anIndex].GetValue();
}
//------------------------------------------------------------------------------
int my1Neural::CreateLinks(void)
{
	if(mWeightCount) return -1; // links exists!

	// create connections
	int cCount = mLayers.size();
	for(int cLoop=1;cLoop<cCount;cLoop++) // skip input layer
	{
		int cCount1 = mLayers[cLoop].size();
		for(int cLoop1=0;cLoop1<cCount1;cLoop1++)
		{
			int cCount2 = mLayers[cLoop-1].size(); // nodecount in prev layer
			for(int cLoop2=0;cLoop2<cCount2;cLoop2++)
			{
				mWeightCount++;
				mLayers[cLoop][cLoop1].AddInput(&mLayers[cLoop-1][cLoop2]);
			}
		}
	}
	return mWeightCount;
}
//------------------------------------------------------------------------------
int my1Neural::RemoveLinks(void)
{
	int cCount = mLayers.size();
	int cPrevSize = mLayers[0].size(), cCurrSize;
	for(int cLoop=1;cLoop<cCount;cLoop++) // skip input layer
	{
		cCurrSize = mLayers[cLoop].size();
		mWeightCount -= cCurrSize*cPrevSize;
		ClearLinks(&mLayers[cLoop]);
		cPrevSize = cCurrSize;
	}
	return mWeightCount;
}
//------------------------------------------------------------------------------
void my1Neural::Evaluate(void)
{
	int cCount = mLayers.size();
	for(int cIndex=1;cIndex<cCount;cIndex++) // skip input layer
	{
		int cCount1 = mLayers[cIndex].size();
		for(int cLoop=0;cLoop<cCount1;cLoop++)
		{
			mLayers[cIndex][cLoop].Evaluate();
		}
	}
}
//------------------------------------------------------------------------------
