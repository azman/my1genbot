//------------------------------------------------------------------------------
#include "my1testbot.hpp"
#include <ctime>
using std::time;
#include <cstdlib>
using std::srand;
using std::rand;
#include <iostream>
using std::cout;
//------------------------------------------------------------------------------
my1TestBot::my1TestBot() : my1BnnBot(VIEW_WIDTH,VIEW_HEIGHT,SAFE_PSDRANGE)
{
	// assign target properties
	mView.target.hue = TARGET_HUE;
	mView.target.thresh = HUE_THRESH;
}
//------------------------------------------------------------------------------
void my1TestBot::CaptureView(void)
{
	// randomize viewer output?
	{
		mView.info.bview = 0;
		mView.info.bside = 0;
		mView.info.bnear = 0;
		mView.info.sidel = rand()%2;
		mView.info.sider = rand()%2;
		mView.info.center = rand()%2;
		mView.info.bpos = 0;
	}
	cout << "Capturing Image...\n";
}
//------------------------------------------------------------------------------
void my1TestBot::CaptureSens(void)
{
	mSens.valid = 0;
	// check validity???
	{
		mSens.valid = 1;
		// get readings
		mSens.f_dist = (rand()/(RAND_MAX))*2*SAFE_PSDRANGE;
		if(mSens.f_dist>mSens.saferange) mSens.f_safe = 1;
		else mSens.f_safe = 0;
		mSens.l_dist = (rand()/(RAND_MAX))*2*SAFE_PSDRANGE;
		if(mSens.l_dist>mSens.saferange) mSens.l_safe = 1;
		else mSens.l_safe = 0;
		mSens.r_dist = (rand()/(RAND_MAX))*2*SAFE_PSDRANGE;
		if(mSens.r_dist>mSens.saferange) mSens.r_safe = 1;
		else mSens.r_safe = 0;
	}
	cout << "Capturing Range...\n";
}
//------------------------------------------------------------------------------
void my1TestBot::MoveDrive(void)
{
	if(mDrive.drive)
	{
		cout << "Driving " << (mDrive.dist) << " unit\n";
	}
	if(mDrive.turn)
	{
		cout << "Turning " << (mDrive.angle) << " unit\n";
	}
}
//------------------------------------------------------------------------------
