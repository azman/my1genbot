//------------------------------------------------------------------------------
#include "my1visbot.hpp"
//------------------------------------------------------------------------------
#define MIN(a,b) (a<b?a:b)
#define MAX(a,b) (a>b?a:b)
//------------------------------------------------------------------------------
my1VisBot::my1VisBot(int aViewX, int aViewY, int aRange)
{
	mViewX = aViewX;
	mViewY = aViewY;
	mBuff = 0x0;
	// initialize range sensor params
	mSens.saferange = aRange;
	mSens.valid = 0;
	// initialize view
	mView.viewer.horizon = mViewY*2/3;
	mView.viewer.left = mViewX/4;
	mView.viewer.right = mViewX - mView.viewer.left;
	mView.viewer.middle = mViewX/2;
	mView.viewer.weight = mViewY/2;
	// target color info should be initialized by app!
}
//------------------------------------------------------------------------------
robosens& my1VisBot::Sens(void)
{
	return mSens;
}
//------------------------------------------------------------------------------
roboview& my1VisBot::View(void)
{
	return mView;
}
//------------------------------------------------------------------------------
void my1VisBot::ColorDirect(void)
{
	int x, y, h;
	int tempx, tempy;
	int bfront, bweight;
	int nearl, nearr, nearc;
	unsigned char *pByte, *pTemp;
	// object info - bfront & bweight used to override in some cases
	mView.info.bpos = 0; mView.info.bview = 0; bfront = 0; bweight = 0;
	mView.info.bside = 0; mView.info.bnear = 0;
	// how close is the object
	nearl = 0; nearr = 0; nearc = 0;
	// try to detect obstacle (NO_HUE) at under view horizons
	mView.info.sidel = 0; mView.info.sider = 0; mView.info.center = 0;
	// exit if no image buffer?
	if(!mBuff) return;
	// setup pointer
	pTemp = (unsigned char*) mBuff;
	pTemp += mViewX*3; // skip first line (border)
	// process image by columns
	for(x=1;x<mViewX-1;x++) // ignore borders?
	{
		pTemp += 3; // skip first column
		pByte = pTemp;
		tempx = 0;
		for(y=1;y<mViewY-1;y++)
		{
			// convert rgb to hue, detect object hue - raw
			h = my1VisBot::RGB2Hue(pByte[0],pByte[1],pByte[2]);
			pByte += mViewX*3; // skip next pixel in column
			if(h!=NO_HUE)
			{
				tempy = h - mView.target.hue;
				if(tempy<0) tempy = -tempy;
				if(tempy>126) tempy = 253 - tempy;
				if(tempy<mView.target.thresh)
				{
					tempx++;
					if(y>mView.viewer.horizon)
					{
						if(x<mView.viewer.left) { if(y>nearl) nearl = y; }
						else if(x<mView.viewer.right) { if(y>nearc) nearc = y; }
						else { if(y>nearr) nearr = y; }
					}
				}
			}
			else // it's an obstacle!
			{
				if(y>mView.viewer.horizon) // closeby obstacle
				{
					if(x<mView.viewer.left) mView.info.sidel++;
					else if(x>=mView.viewer.right) mView.info.sider++;
					else mView.info.center++;
				}
			}
		}
		if(tempx>mView.info.bview)
		{
			mView.info.bview = tempx;
			mView.info.bpos = x;
		}
		if(x>=mView.viewer.left&&x<mView.viewer.right)
		{
			if(tempx>=mView.viewer.weight&&tempx>bweight)
			{ // acceptable weight to be considered as in front!
				bfront = x; bweight = tempx;
			}
		}
	}
	// encode object info - if found!
	if(mView.info.bpos>0)
	{
		if(mView.info.bpos<mView.viewer.left)
			mView.info.bside = OBJECT_LEFT;
		else if(mView.info.bpos<mView.viewer.right)
			mView.info.bside = OBJECT_CENTER;
		else
			mView.info.bside = OBJECT_RIGHT;
		// override condition - based on weight factor (see above)
		if(bfront) mView.info.bside = OBJECT_CENTER;
		// put position to offset 0 as view center, -ve is left side
		mView.info.bpos -= mView.viewer.middle;
		// check closeby located target
		if(mView.info.bview>=mView.viewer.weight&&(nearc||nearl||nearr))
			mView.info.bnear = MAX(nearr,MAX(nearl,nearc));
		else
			mView.info.bnear = 0;
	}
	// could object overlapped obst pixels?
	if(nearr) mView.info.sider = 0;
	if(nearl) mView.info.sidel = 0;
	if(nearc) mView.info.center = 0;
}
//------------------------------------------------------------------------------
int my1VisBot::RGB2Hue(unsigned char r, unsigned char g, unsigned char b)
{
	int hue, delta, max, min;
	max   = MAX(r, MAX(g,b));
	min   = MIN(r, MIN(g,b));
	delta = max - min;
	if(2*delta <= max) // delta/max ~= 0.0
		return NO_HUE;
	if (r==max) hue = 42 + 42*(g-b)/delta; // red to yellow
	else if (g==max) hue = 126 +42*(b-r)/delta; // green to cyan
	else if (b==max) hue = 210 +42*(r-g)/delta; // blue t magenta
	else hue = 0;
	return hue; // max at 252 (=360 deg.)
}
//------------------------------------------------------------------------------
