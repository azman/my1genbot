//------------------------------------------------------------------------------
#ifndef __MY1BNNBOTH__
#define __MY1BNNBOTH__
//------------------------------------------------------------------------------
// behavior-based control using neural network
#include "my1bhvbot.hpp"
#include "my1neural.hpp"
//------------------------------------------------------------------------------
// neunet settings
// => inputs: roboview{bview,bpos(bside),bnear,sider,sidel,center}
//		robosens{r_safe,l_safe,f_safe}
// => hidden: trial & error
// => output: robomove{dist, angle}
#define NNET_HLAYER 1
#define NNET_INNODE 9
#define NNET_HDNODE 9
#define NNET_OPNODE 2
//------------------------------------------------------------------------------
// constant for robomove decoder
#define ABS_MOVE 0.25
#define ABS_TURN 0.25
// constant for behavior factor usage
#define BHV_ACTIVE_FACTOR 1.0
#define BHV_OVERRIDE_FACTOR 0.5
#define BHV_INACTIVE_FACTOR 0.0
// constant for max turncount
#define BNN_MAX_TURN 5
//------------------------------------------------------------------------------
// behavior-based on neunet controller
class my1BnnBot : public my1BhvBot
{
protected:
	my1Neural* mNeural;
	float *pVectorI, *pVectorO; // user must provide space using SetVectors()!
	bool mUseBhvFactor;
	int mTurnCount;
	int mPrevMove, mPrevTurn;
	my1BhvAvoid mBhvAvoid;
	my1BhvScout mBhvScout;
	my1BhvDrive mBhvDrive;
public:
	my1BnnBot(int,int,int); // x,y,range
	int SetNeural(my1Neural*);
	my1Neural* GetNeural(void);
	void SetVectors(float*,float*);
	bool CheckVectors(void);
	void UseBhvFactor(bool);
	bool UsingBhvFactor(void);
	void GetInputVector(float*); //  user must create space
	// virtual functions - can be override
	virtual int ErrorDrive(void); // checks & rectify errornous drive
	virtual int ZeroDrive(void);
	virtual int Evaluate(void);
	// static utility functions - 1 copy for all
	static void EncodeMoveVector(robomove& aMove, float* aVector,
		float aFactor=1.0);
	static void DecodeMoveVector(robomove& aMove, float* aVector);
};
//------------------------------------------------------------------------------
#endif
//------------------------------------------------------------------------------
