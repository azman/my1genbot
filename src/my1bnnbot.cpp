//------------------------------------------------------------------------------
#include "my1bnnbot.hpp"
//------------------------------------------------------------------------------
my1BnnBot::my1BnnBot(int aViewX, int aViewY, int aRange)
	: my1BhvBot(aViewX, aViewY, aRange)
{
	mNeural = 0x0;
	mUseBhvFactor = true; // default
	mTurnCount = 0;
	mPrevMove = MOVE_0;
	mPrevTurn = TURN_0;
	// setup behaviors
	this->AddBehavior(&mBhvAvoid);
	this->AddBehavior(&mBhvScout);
	this->AddBehavior(&mBhvDrive);
}
//------------------------------------------------------------------------------
int my1BnnBot::SetNeural(my1Neural* aNeural)
{
	int cResult = -1;
	if(aNeural)
	{
		cResult = aNeural->WeightCount();
		if(cResult) mNeural = aNeural;
	}
	return cResult;
}
//------------------------------------------------------------------------------
my1Neural* my1BnnBot::GetNeural(void)
{
	return mNeural;
}
//------------------------------------------------------------------------------
void my1BnnBot::SetVectors(float* aVectorI, float* aVectorO)
{
	pVectorI = aVectorI;
	pVectorO = aVectorO;
}
//------------------------------------------------------------------------------
bool my1BnnBot::CheckVectors(void)
{
	if(pVectorI&&pVectorO) return true;
	else return false;
}
//------------------------------------------------------------------------------
void my1BnnBot::UseBhvFactor(bool aTrue)
{
	mUseBhvFactor = aTrue;
}
//------------------------------------------------------------------------------
bool my1BnnBot::UsingBhvFactor(void)
{
	return mUseBhvFactor;
}
//------------------------------------------------------------------------------
void my1BnnBot::GetInputVector(float* aVector)
{
	aVector[0] = (float) mView.info.bview;
	aVector[1] = (float) mView.info.bpos; // bside;
	aVector[2] = (float) mView.info.bnear;
	aVector[3] = (float) mView.info.sidel;
	aVector[4] = (float) mView.info.sider;
	aVector[5] = (float) mView.info.center;
	aVector[6] = (float) mSens.f_safe;
	aVector[7] = (float) mSens.l_safe;
	aVector[8] = (float) mSens.r_safe;
}
//------------------------------------------------------------------------------
int my1BnnBot::ErrorDrive(void)
{
	int cResult = 0;
	if(mView.info.center) // obstacle ahead!!
	{
		if(mView.info.sidel||mView.info.sider) // also at least 1 side
		{
			if(mDrive.drive==MOVE_F)
			{   // are we running into obstacle?
				mDrive.drive = MOVE_0;
				mDrive.dist = 0.0;
				cResult = 1;
			}
		}
	}
	else // clear ahead!!
	{
		if(!mView.info.sidel&&!mView.info.sider) // absolutely clear!
		{
			if(mView.info.bview) // now we check for ball related error
			{
				if(mDrive.turn!=mView.info.bside)
				{   // are we moving away from ball??
					mDrive.turn = mView.info.bside;
					mDrive.angle = (float) mDrive.turn*0.25;
					cResult = 1;
				}
				if(mDrive.turn==TURN_0&&mDrive.drive==MOVE_0)
				{   // are we not moving towards ball??
					mDrive.drive = MOVE_F;
					mDrive.dist = 1.0;
					cResult = 1;
				}
			}
			else // now error in scout behavior??
			{
				if(mDrive.drive!=MOVE_F)
				{   // are we stopping for nothing?
					mDrive.drive = MOVE_F;
					mDrive.dist = 1.0;
					cResult = 1;
				}
			}
		}
		else // at least one side has obstacle!
		{
			if(mView.info.sidel&&mDrive.turn==TURN_L)
			{ // are we turning towards obstacle??
				mDrive.turn = TURN_R;
				mDrive.angle = mDrive.turn*1.0;
				cResult = 1;
			}
			else if(mView.info.sider&&mDrive.turn==TURN_R)
			{ // are we turning towards obstacle??
				mDrive.turn = TURN_L;
				mDrive.angle = mDrive.turn*1.0;
				cResult = 1;
			}
		}
	}
	// totally stopped?? major error?
	if(mDrive.drive==MOVE_0&&mDrive.turn==TURN_0)
	{
		mDrive.turn = TURN_R; // default turn right
		mDrive.angle = -1.0;
		cResult = 1;
	}
	// going round & round is accumulated error!
	if(cResult==0) // only if no error detected
	{
		if(mPrevMove==mDrive.drive&&
			(mPrevTurn!=TURN_0&&mPrevTurn==mDrive.turn))
		{
			mTurnCount++;
			if(mTurnCount==BNN_MAX_TURN)
			{
				cResult = mTurnCount;
				mDrive.turn = -mDrive.turn; // try turn the other way
				mDrive.angle = -mDrive.angle;
				mTurnCount = 0;
			}
		}
		else
		{
			mTurnCount = 0;
		}
	}
	else
	{
		mTurnCount = 0;
	}
	return cResult;
}
//------------------------------------------------------------------------------
int my1BnnBot::ZeroDrive(void)
{
	int cResult = 0;
	if(mView.info.bview&&!mView.info.bside&&mView.info.bnear)
	{
		mDrive.drive = MOVE_0;
		mDrive.turn = TURN_0;
		cResult = 1;
	}
	return cResult;
}
//------------------------------------------------------------------------------
int my1BnnBot::Evaluate(void)
{
	float *pVector;
	float cFactor;
	bool cActivated = false; // once activated, the rest are suppressed!
	robomove cMove;
	my1Behavior* pSelect = &mDefaultBehavior;

	if(!mNeural) return my1BhvBot::Evaluate();

	// gather input - make sure factor is set if using it
	pVector = pVectorI;
	GetInputVector(pVector);
	pVector += NNET_INNODE;

	// input from behavior outputs
	list<my1Behavior*>::iterator pBehavior = mBhvList.begin();
	while(pBehavior!=mBhvList.end())
	{
		pSelect = *pBehavior;
		pSelect->Evaluate();
		if(cActivated)pSelect->SetFactor(BHV_INACTIVE_FACTOR);
		else if(pSelect->IsActive())
		{
			pSelect->SetFactor(BHV_ACTIVE_FACTOR);
			cActivated = true;
		}
		else pSelect->SetFactor(BHV_OVERRIDE_FACTOR);
		if(mUseBhvFactor) cFactor = pSelect->GetFactor();
		else cFactor = 1.0;
		EncodeMoveVector(pSelect->Drive(),pVector,cFactor);
		pVector += NNET_OPNODE;
		pBehavior++;
	}

	// evaluate neural
	mNeural->SetInput(pVectorI);
	mNeural->Evaluate();
	mNeural->GetOutput(pVectorO);
	// decode output
	DecodeMoveVector(cMove,pVectorO);
	// save previous move
	mPrevMove = mDrive.drive;
	mPrevTurn = mDrive.turn;
	// assign new move
	this->Drive(cMove);

	return pSelect->GetBehaveID();
}
//------------------------------------------------------------------------------
void my1BnnBot::EncodeMoveVector(robomove& aMove, float* aVector, float aFactor)
{
	aVector[0] = aMove.dist*aFactor;
	aVector[1] = aMove.angle*aFactor;
}
//------------------------------------------------------------------------------
void my1BnnBot::DecodeMoveVector(robomove& aMove, float* aVector)
{
	aMove.dist = (aVector[0]*2.0)-1.0; // range -1 to 1
	aMove.angle = (aVector[1]*2.0)-1.0;
	/* check drive info */
	if(aMove.dist>ABS_MOVE)
	{
		aMove.drive = MOVE_F;
		//if(aMove.dist>0.75) aMove.dist = 1.0;
		//else aMove.dist = 0.5;
	}
	else if(aMove.dist<-ABS_MOVE)
	{
		aMove.drive = MOVE_R;
		//if(aMove.dist<-0.75) aMove.dist = -1.0;
		//else aMove.dist = -0.5;
	}
	else
	{
		aMove.drive = MOVE_0;
		aMove.dist = 0.0;
	}
	if(aMove.angle>ABS_TURN)
	{
		aMove.turn = TURN_L;
		//if(aMove.angle>0.75) aMove.angle = 1.0;
		//else aMove.angle = 0.5;
	}
	else if(aMove.angle<-ABS_TURN)
	{
		aMove.turn = TURN_R;
		//if(aMove.angle<-0.75) aMove.angle = -1.0;
		//else aMove.angle = -0.5;
	}
	else
	{
		aMove.turn = TURN_0;
		aMove.angle = 0.0;
	}
}
//------------------------------------------------------------------------------
