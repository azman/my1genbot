# makefile for my1genbot - Genetic Robot code library?

PROJECT = my1genbot
MAINBIN = $(PROJECT)
MAINPRO = $(MAINBIN)
MAINOBJ = my1mobbot.o my1visbot.o my1bhvbot.o my1neural.o my1bnnbot.o
MAINOBJ += my1testbot.o $(MAINBIN).o
# maybe write a bot sim later?
#TOOLPRO = my1botsim
#TOOLBIN = $(TOOLPRO)
#TOOLOBJ = $(TOOLPRO).o

# do not need this - unless add code for learning?
EXTPATH = ../my1genetic/src

DELETE = rm -rf
CFLAGS += -Wall --static
LFLAGS +=
OFLAGS +=
ifeq ($(OS),Windows_NT)
	MAINPRO = $(PROJECT).exe
	CFLAGS += -DDO_MINGW
	ifneq ($(shell echo),)
		DELETE = del
	endif
endif

CC = gcc
CPP = g++
debug: CFLAGS += -DMY1DEBUG

all: main

main: $(MAINPRO)

#tool: $(TOOLPRO)

new: clean all

debug: new

$(MAINPRO): $(MAINOBJ)
	$(CPP) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS)

#$(TOOLPRO): $(TOOLOBJ)
#	$(CPP) $(CFLAGS) -o $@ $+ $(LFLAGS) $(OFLAGS)

%.o: src/%.cpp src/%.hpp
	$(CPP) $(CFLAGS) -c $<

%.o: src/%.cpp
	$(CPP) $(CFLAGS) -c $<

%.o: $(EXTPATH)/%.cpp $(EXTPATH)/%.hpp
	$(CC) $(CFLAGS) -c $<

# just in case needs c code!
%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) -c $<

clean:
	-$(DELETE) $(MAINPRO) $(MAINOBJ) *.o
